#!/bin/bash

. scripts/utils.sh

function createDockerNetwork() {
  DOCKER_SOCK="${DOCKER_SOCK}" docker network create payment_demo
}

function removeDockerNetwork() {
  DOCKER_SOCK="${DOCKER_SOCK}" docker network rm payment_demo
}

function toolsExec() {
  DOCKER_SOCK="${DOCKER_SOCK}" docker run \
  -e FABRIC_CFG_PATH=/go/payment-demo/config \
  -e CORE_PEER_LOCALMSPID=${CORE_PEER_LOCALMSPID} \
  -e CORE_PEER_MSPCONFIGPATH=${CORE_PEER_MSPCONFIGPATH} \
  -e CORE_PEER_MSPCONFIGPATH=${CORE_PEER_MSPCONFIGPATH} \
  -e CORE_PEER_ADDRESS=${CORE_PEER_ADDRESS} \
  --rm --network=payment_demo -v ${PWD}:/go/payment-demo -w /go/payment-demo \
  hyperledger/fabric-tools:2.3 "$@"
}

function createOrgs() {
  infoln "Generating certificates using cryptogen tool"

  set -x
  toolsExec cryptogen generate --config=crypto-config.yaml --output="organizations"
  res=$?
  { set +x; } 2>/dev/null
  if [ $res -ne 0 ]; then
    fatalln "Failed to generate certificates..."
  fi
}

# Bring up the peer and orderer nodes using docker compose.
function networkUp() {
#  checkPrereqs
  # generate artifacts if they don't exist
  if [ ! -d "organizations/peerOrganizations" ] || [ ! -d "organizations/peerOrganizations" ]; then
    createOrgs
  fi

  DOCKER_SOCK="${DOCKER_SOCK}" docker-compose -f ./docker/docker-compose.yaml up -d 2>&1

  docker ps
  if [ $? -ne 0 ]; then
    fatalln "Unable to start network"
  fi
}

function servicesUp() {
  DOCKER_SOCK="${DOCKER_SOCK}" docker-compose -f ./docker/docker-compose-b2bchain.yaml up -d 2>&1
}

# Create channel
function createChannel() {
  infoln "Creating channel"

  set -x
  toolsExec configtxgen -profile PaymentDemoGenesis -outputBlock channel/demo.block -channelID demo
  res=$?
  { set +x; } 2>/dev/null
  if [ $res -ne 0 ]; then
    fatalln "Failed to generate channel genesis block"
  fi

  set -x
  toolsExec osnadmin channel join -c demo -b ./channel/demo.block -o orderer.example.com:7053
  res=$?
  { set +x; } 2>/dev/null
  if [ $res -ne 0 ]; then
    fatalln "Failed to create channel"
  fi
}

function switchToOrg() {
  ORG="$1"
  USERNAME="${2:-Admin}"
  if [[ ${ORG} == "Seller" ]]; then
    export CORE_PEER_LOCALMSPID="SellerMSP"
    export CORE_PEER_MSPCONFIGPATH=/go/payment-demo/organizations/peerOrganizations/seller.example.com/users/${USERNAME}@seller.example.com/msp/
    export CORE_PEER_ADDRESS=peer0.seller.example.com:7051
  elif [[ ${ORG} == "Buyer" ]]; then
    export CORE_PEER_LOCALMSPID="BuyerMSP"
    export CORE_PEER_MSPCONFIGPATH=/go/payment-demo/organizations/peerOrganizations/buyer.example.com/users/${USERNAME}@buyer.example.com/msp/
    export CORE_PEER_ADDRESS=peer0.buyer.example.com:9051
  elif [[ ${ORG} == "Bank" ]]; then
    export CORE_PEER_LOCALMSPID="BankMSP"
    export CORE_PEER_MSPCONFIGPATH=/go/payment-demo/organizations/peerOrganizations/bank.example.com/users/${USERNAME}@bank.example.com/msp/
    export CORE_PEER_ADDRESS=peer0.bank.example.com:11051
  else
    fatalln "Unknown organization ${ORG}"
  fi
}

function joinOrgToChannel() {
  local ORG="$1"
  switchToOrg ${ORG}

  set -x
  toolsExec peer channel fetch 0 -c demo -o orderer.example.com:7050 demo_0.block
  res=$?
  { set +x; } 2>/dev/null
  if [ $res -ne 0 ]; then
    fatalln "Failed to fetch config block"
  fi

  set -x
  toolsExec peer channel join -b demo_0.block
  res=$?
  { set +x; } 2>/dev/null
  if [ $res -ne 0 ]; then
    fatalln "Failed to join to channel"
  fi
}

# Join organizations to channel
function joinOrgsToChannel() {
  joinOrgToChannel "Seller"
  joinOrgToChannel "Buyer"
  joinOrgToChannel "Bank"
}

function installCCPackageByOrg() {
  local ORG="$1"

  infoln "Install chaincode for ${ORG} organization"
  switchToOrg ${ORG}
  set -x
  toolsExec peer lifecycle chaincode install ./chaincode/payment.tar.gz
  res=$?
  { set +x; } 2>/dev/null
  if [ $res -ne 0 ]; then
    fatalln "Failed to install chaincode package"
  fi

  set -x
  toolsExec peer lifecycle chaincode approveformyorg  -o orderer.example.com:7050 \
    --channelID demo --name payment --version 1.0 --init-required \
    --package-id payment:38175a0f50c05738cc5721c9e309b2e836544edd66b9b0c4ba797796ca4a9f7d \
    --sequence "1"
#    --signature-policy "AND ('SellerMSP.peer','BuyerMSP.peer', 'BankMSP.peer')"

  res=$?
  { set +x; } 2>/dev/null
  if [ $res -ne 0 ]; then
    fatalln "Failed to install chaincode package"
  fi
}

function installCCPackage() {
  installCCPackageByOrg "Seller"
  sleep 1
  installCCPackageByOrg "Buyer"
  sleep 1
  installCCPackageByOrg "Bank"
  sleep 1
  commitChaincode "Bank"
  sleep 1
  invokeChaincodeInit "Seller"
}

function commitChaincode() {
  local ORG="$1"
  infoln "Commit chaincode definition to the channel"
  switchToOrg ${ORG}

  set -x
  toolsExec peer lifecycle chaincode commit -o orderer.example.com:7050 \
    --channelID demo --name payment --version 1.0 --sequence 1 --init-required \
    --peerAddresses peer0.seller.example.com:7051 --peerAddresses peer0.buyer.example.com:9051 --peerAddresses peer0.bank.example.com:11051 >&log.txt
  res=$?
  { set +x; } 2>/dev/null
  cat log.txt
  verifyResult $res "Chaincode definition commit failed"
  successln "Chaincode definition committed on the channel"
}

function invokeChaincodeInit() {
  local ORG="$1"
  infoln "Init chaincode"
  switchToOrg ${ORG} "User1"

  set -x
  infoln "Init chaincode"
  toolsExec peer chaincode invoke -o orderer.example.com:7050 \
    -C demo -n payment --isInit -c '{"function":"init","Args":[]}' \
    --peerAddresses peer0.seller.example.com:7051 \
    --peerAddresses peer0.buyer.example.com:9051 \
    --peerAddresses peer0.bank.example.com:11051 >&log.txt
  res=$?
  { set +x; } 2>/dev/null
  cat log.txt
  verifyResult $res "Init chaincode failed"
  successln "Chaincode initiated successfully"
}

function createOrganizations() {
  #  render bank registration payload
  infoln "Create bank..."
  BANK_USER_CERT=$(cat ./organizations/peerOrganizations/bank.example.com/users/User1@bank.example.com/msp/signcerts/User1@bank.example.com-cert.pem)
  export BANK_USER_CERT=${BANK_USER_CERT//$'\n'/\\n}
  BANK_USER_CA_CERT=$(cat ./organizations/peerOrganizations/bank.example.com/users/User1@bank.example.com/msp/cacerts/ca.bank.example.com-cert.pem)
  export BANK_USER_CA_CERT=${BANK_USER_CA_CERT//$'\n'/\\n}
  envsubst < ./samples/create-bank.json.tmpl > ./samples/create-bank.json

  set -x
  curl --location --request POST 'localhost:18081/org' \
    --header 'Content-Type: application/json' \
    --data '@./samples/create-bank.json'
  res=$?
  { set +x; } 2>/dev/null
  if [ $res -ne 0 ]; then
    fatalln "Failed to create bank"
  fi
  sleep 3

  # render seller registration payload
  infoln ""
  infoln "Create seller..."
  SELLER_USER_CERT=$(cat ./organizations/peerOrganizations/seller.example.com/users/User1@seller.example.com/msp/signcerts/User1@seller.example.com-cert.pem)
  export SELLER_USER_CERT=${SELLER_USER_CERT//$'\n'/\\n}
  SELLER_USER_CA_CERT=$(cat ./organizations/peerOrganizations/seller.example.com/users/User1@seller.example.com/msp/cacerts/ca.seller.example.com-cert.pem)
  export SELLER_USER_CA_CERT=${SELLER_USER_CA_CERT//$'\n'/\\n}
  envsubst < ./samples/create-seller.json.tmpl > ./samples/create-seller.json

  set -x
  curl --location --request POST 'localhost:18081/org' \
    --header 'Content-Type: application/json' \
    --data '@./samples/create-seller.json'
  res=$?
  { set +x; } 2>/dev/null
  if [ $res -ne 0 ]; then
    fatalln "Failed to create seller"
  fi
  sleep 3

  # render buyer registration payload
  infoln ""
  infoln "Create buyer..."
  BUYER_USER_CERT=$(cat ./organizations/peerOrganizations/buyer.example.com/users/User1@buyer.example.com/msp/signcerts/User1@buyer.example.com-cert.pem)
  export BUYER_USER_CERT=${BUYER_USER_CERT//$'\n'/\\n}
  BUYER_USER_CA_CERT=$(cat ./organizations/peerOrganizations/buyer.example.com/users/User1@buyer.example.com/msp/cacerts/ca.buyer.example.com-cert.pem)
  export BUYER_USER_CA_CERT=${BUYER_USER_CA_CERT//$'\n'/\\n}
  envsubst < ./samples/create-buyer.json.tmpl > ./samples/create-buyer.json

  set -x
  curl --location --request POST 'localhost:18081/org' \
    --header 'Content-Type: application/json' \
    --data '@./samples/create-buyer.json'
  res=$?
  { set +x; } 2>/dev/null
  if [ $res -ne 0 ]; then
    fatalln "Failed to create buyer"s
  fi
  sleep 3

  # create payment agreement
  infoln ""
  infoln "Create payment agreement..."
  set -x
  curl --location --request POST 'localhost:18081/agreement' \
    --header 'Content-Type: application/json' \
    --data '@./samples/create-agreement.json'
  res=$?
  { set +x; } 2>/dev/null
  if [ $res -ne 0 ]; then
    fatalln "Failed to create payment agreement"
  fi
  sleep 3

  # create payment agreement
  infoln ""
  infoln "Create payment template..."
  set -x
  curl --location --request POST 'localhost:18081/template' \
    --header 'Content-Type: application/json' \
    --data '@./samples/create-payment-template.json'
  res=$?
  { set +x; } 2>/dev/null
  if [ $res -ne 0 ]; then
    fatalln "Failed to create payment template"
  fi
  sleep 3

  # create payment agreement
  infoln ""
  infoln "Confirm payment template..."
  set -x
  curl --location --request POST 'localhost:28081/template/confirmation' \
    --header 'Content-Type: application/json' \
    --data '@./samples/confirm-template.json'
  res=$?
  { set +x; } 2>/dev/null
  if [ $res -ne 0 ]; then
    fatalln "Failed to confirm payment template"
  fi
}

function createPayment() {
  export PAYMENT_ORDER_ID=$1
  export PAYMENT_ORDER_AMOUNT="${2:-100000}"

  infoln "Create new payment ${PAYMENT_ORDER_ID} with amount ${PAYMENT_ORDER_AMOUNT}"

  envsubst < ./samples/create-payment.json.tmpl > ./samples/create-payment.json

  set -x
  curl --location --request POST 'localhost:28081/payment' \
    --header 'Content-Type: application/json' \
    --data '@./samples/create-payment.json'
  res=$?
  { set +x; } 2>/dev/null
  if [ $res -ne 0 ]; then
    fatalln "Failed to create payment"
  fi
}

function debitRequest() {
  export PAYMENT_ORDER_ID=$1
  export PAYMENT_ORDER_AMOUNT="${2:-100000}"

  infoln "Debit request for payment ${PAYMENT_ORDER_ID} with amount ${PAYMENT_ORDER_AMOUNT}"

  envsubst < ./samples/debit-request.json.tmpl > ./samples/debit-request.json

  curl --location --request POST 'localhost:18081/payment/debit-request' \
    --header 'Content-Type: application/json' \
    --data '@./samples/debit-request.json'
  res=$?
  { set +x; } 2>/dev/null
  if [ $res -ne 0 ]; then
    fatalln "Failed to debit request"
  fi
}

# Tear down running network
function networkDown() {
  DOCKER_SOCK=$DOCKER_SOCK docker-compose -f ./docker/docker-compose.yaml down --volumes --remove-orphans
}

function servicesDown() {
  DOCKER_SOCK=$DOCKER_SOCK docker-compose -f ./docker/docker-compose-b2bchain.yaml down --volumes --remove-orphans
}

# Parse commandline args

## Parse mode
if [[ $# -lt 1 ]] ; then
  printHelp
  exit 0
else
  MODE=$1
  shift
fi

# Determine mode of operation and printing out what we asked for
if [ "$MODE" == "up" ]; then
  createDockerNetwork
  infoln "Starting network"
  networkUp
  sleep 5
  createChannel
  sleep 5
  joinOrgsToChannel
  installCCPackage
  sleep 5
  infoln "Starting services"
  servicesUp
  sleep 5
  infoln "Create organizations"
  createOrganizations
elif [ "$MODE" == "down" ]; then
  infoln "Stopping services"
  servicesDown
  infoln "Stopping network"
  networkDown
  removeDockerNetwork
elif [ "$MODE" == "createPayment"  ]; then
  createPayment $1 $2
elif [ "$MODE" == "debitRequest"  ]; then
  debitRequest $1 $2
else
  printHelp
  exit 1
fi
